/**
 * A dataset is the main ressource of the application.
 * It handles a coherent set of data, retrieved from local file or remote source.
 * It contains several {@link Feature|features}, each one being associated with pictures.
 * 
 * This class should be overidden in order to handle each specific file or source.
 * @abstract
 */
class Dataset {
	constructor() {
		if(this.constructor.name === "Dataset") {
			throw new TypeError("Dataset should not be called directly. Use instead one of the subclasses.");
		}
		
		this.features = [];
	}
	
	/**
	 * The default radius for pictures search.
	 * @private
	 */
	_getDefaultRadius() {
		return 20;
	}
	
	/**
	 * Get the whole set of features.
	 * @return {Promise} A promise solving on the array of {@link Feature|features}
	 */
	getAllFeatures() {
		return new Promise(resolve => { resolve(this.features); });
	}
}

module.exports = Dataset;
