const Dataset = require('../Dataset');
const Feature = require('../Feature');
const Hash = require('object-hash');
const jsts = require('jsts');

/**
 * An GeoJSON {@link Dataset} is a set of features in GeoJSON format.
 * 
 * @param {Object} geojson The GeoJSON FeatureCollection, as string or object
 * @param {Object} [options] Options for data retrieval
 */
class GeoJSONDataset extends Dataset {
	constructor(geojson, options) {
		super();
		this.options = options || {};
		
		if(typeof geojson === "string") {
			geojson = JSON.parse(geojson);
		}
		
		if(geojson.type !== "FeatureCollection" || !geojson.features || geojson.features.length === 0) {
			throw new TypeError("Not a GeoJSON FeatureCollection");
		}
		
		this.id = Hash(geojson);
		const reader = new jsts.io.GeoJSONReader();
		
		this.features = geojson.features.map((f,i) => {
			if(f.type !== "Feature") { throw new TypeError("One of the features is not properly defined"); }
			
			const geom = reader.read(f.geometry);
			if(!geom.isValid()) { throw new TypeError("One of the feature's geometry is invalid"); }
			
			const coords = geom.getCentroid().getCoordinate();
			const id = f.properties.id || Hash(coords);
			
			return new Feature(id, [ coords.y, coords.x ], f.properties, null, geom);
		});
	}
	
	/**
	 * Override of {@link Dataset#getAllFeatures}
	 */
	getAllFeatures() {
		return new Promise(resolve => resolve(this.features));
	}
}

module.exports = GeoJSONDataset;
