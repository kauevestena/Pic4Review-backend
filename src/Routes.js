const CONFIG = require('../config.json');

/**
 * Routes for the API.
 * Redirects to appropriate RouteHandler.
 */
module.exports = function(app) {
	const dft = require('./ctrl/DefaultRouteHandler');
	const missions = require('./ctrl/MissionsRouteHandler');
	const features = require('./ctrl/MissionsFeaturesRouteHandler');
	const pictures = require('./ctrl/PicturesRouteHandler');
	const users = require('./ctrl/UsersRouteHandler');
	
	/**
	 * @api {get} / API status
	 * @apiDescription Get the current working status of the API
	 * @apiName GetStatus
	 * @apiGroup Default
	 * 
	 * @apiSuccess {String} status The response status (OK)
	 */
	app.route('/').get(dft.status);
	
	/**
	 * @api {get} /missions Missions list
	 * @apiDescription List of all missions according to given criterias
	 * @apiName GetMissions
	 * @apiGroup Missions
	 * 
	 * @apiParam {int} [page] The page number (starting and defaults to 1)
	 * @apiParam {String} [type] The kind of missions
	 * @apiParam {String} [theme] The missions theme
	 * @apiParam {String} [sort] How to sort results (pertinence, newest, oldest, least-complete, most-complete)
	 * @apiParam {String} [user] The user ID, to retrieve only missions created by this user
	 * @apiParam {String} [contributor] The user ID, to retrieve only missions this user contributed to
	 */
	app.route('/missions').get(missions.list);
	
	/**
	 * @api {post} /missions New mission
	 * @apiDescription Create a new mission
	 * @apiName PostMission
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} type The kind of mission (fix, improve, integrate)
	 * @apiParam {String} theme The mission theme
	 * @apiParam {Int} [maxYears] The max age in years a picture shall have to be considered 
	 * @apiParam {String} areaname The area name (as City, Country, Continent)
	 * @apiParam {String} shortdesc A short description of the task (around 50 characters)
	 * @apiParam {String} fulldesc A complete description of the task. Supports Markdown
	 * @apiParam {String} datatype The kind of dataset (osmose)
	 * @apiParam {Object} dataoptions Options for the dataset
	 * @apiParam {Number} minlat Minimal latitude (WGS84)
	 * @apiParam {Number} minlon Minimal longitude (WGS84)
	 * @apiParam {Number} maxlat Maximal latitude (WGS84)
	 * @apiParam {Number} maxlon Maximal longitude (WGS84)
	 * @apiParam {String} username The OSM user name
	 * @apiParam {String} userid The OSM user ID
	 */
	app.route('/missions').post(missions.create);
	
	/**
	 * @api {get} /missions/loading Mission creation status
	 * @apiDescription Status of mission creation
	 * @apiName GetMissionLoading
	 * @apiGroup Missions
	 * 
	 * @apiParam {int} pictoken The mission loading token
	 */
	app.route('/missions/loading').get(missions.loading);
	
	/**
	 * @api {get} /missions/map Missions map data
	 * @apiDescription List of missions according to criterias, for map rendering
	 * @apiName GetMissionsMap
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} [type] The kind of missions
	 * @apiParam {String} [theme] The missions theme
	 * @apiParam {String} [user] The user ID, to retrieve only missions created by this user
	 * @apiParam {String} [contributor] The user ID, to retrieve only missions this user contributed to
	 */
	app.route('/missions/map').get(missions.map);
	
	/**
	 * @api {post} /missions/preview Preview mission
	 * @apiDescription Preview mission features
	 * @apiName PostMissionPreview
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} datatype The kind of dataset (osmose)
	 * @apiParam {Object} dataoptions Options for the dataset
	 * @apiParam {Number} minlat Minimal latitude (WGS84)
	 * @apiParam {Number} minlon Minimal longitude (WGS84)
	 * @apiParam {Number} maxlat Maximal latitude (WGS84)
	 * @apiParam {Number} maxlon Maximal longitude (WGS84)
	 */
	app.route('/missions/preview').post(missions.preview);
	
	/**
	 * @api {get} /missions/templates Missions templates list
	 * @apiDescription List of all missions templates according to criterias
	 * @apiName GetMissionsTemplates
	 * @apiGroup Missions
	 */
	app.route('/missions/templates').get(missions.templates);
	
	/**
	 * @api {get} /missions/:mid Mission details
	 * @apiDescription Full details about a given mission
	 * @apiName GetMission
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {String} [userid] The OSM user ID (if given, check if user can edit)
	 */
	app.route('/missions/:mid').get(missions.details);
	
	/**
	 * @api {put} /missions/:mid Update mission
	 * @apiDescription Edit given mission
	 * @apiName PutMission
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {String} [type] The kind of mission (fix, improve, integrate)
	 * @apiParam {String} [theme] The mission theme
	 * @apiParam {Int} [maxYears] The max age in years a picture shall have to be considered 
	 * @apiParam {String} [areaname] The area name (as City, Country, Continent)
	 * @apiParam {String} [shortdesc] A short description of the task (around 50 characters)
	 * @apiParam {String} [fulldesc] A complete description of the task. Supports Markdown
	 * @apiParam {String} [status] The new status
	 * @apiParam {String} userid The OSM user ID
	 */
	app.route('/missions/:mid').put(missions.update);
	
	/**
	 * @api {get} /missions/:mid/features Features of a mission
	 * @apiDescription Features and their status
	 * @apiName GetMissionFeatures
	 * @apiGroup Features
	 * 
	 * @apiParam {String} mid The mission ID
	 */
	app.route('/missions/:mid/features').get(features.list);
	
	/**
	 * @api {get} /missions/:mid/features/next Next feature
	 * @apiDescription Next feature to review for this mission
	 * @apiName GetMissionNextFeature
	 * @apiGroup Features
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {float} [lat] Latitude to search next to
	 * @apiParam {float} [lng] Longitude to search next to
	 * @apiParam {String} [userid] The user ID
	 */
	app.route('/missions/:mid/features/next').get(features.next);
	
	/**
	 * @api {get} /missions/:mid/export/missing Features missing pictures
	 * @apiDescription Features without decent pictures
	 * @apiName GetMissionExportMissing
	 * @apiGroup Features
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {String} format The export format (gpx, geojson, kml)
	 */
	app.route('/missions/:mid/export/missing').get(features.exportMissing);
	
	/**
	 * @api {put} /missions/:mid/features/:fid Update feature
	 * @apiDescription Update given feature
	 * @apiName PutMissionFeature
	 * @apiGroup Features
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {String} fid The feature ID
	 * @apiParam {String} username The OSM user name
	 * @apiParam {String} userid The OSM user ID
	 * @apiParam {String} status The new status
	 */
	app.route('/missions/:mid/features/:fid').put(features.update);
	
	/**
	 * @api {get} /missions/:mid/stats Mission statistics
	 * @apiDescription Statistics of the given mission
	 * @apiName GetMissionStats
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} mid The mission ID
	 */
	app.route('/missions/:mid/stats').get(missions.stats);
	
	/**
	 * @api {get} /missions/:mid/stats/:uid Mission statistics for user
	 * @apiDescription Statistics of the given mission for a particular user
	 * @apiName GetMissionUserStats
	 * @apiGroup Missions
	 * 
	 * @apiParam {String} mid The mission ID
	 * @apiParam {String} uid The user ID
	 */
	app.route('/missions/:mid/stats/:uid').get(missions.levelup);
	
	/**
	 * @api {get} /users/:uid/stats User statistics
	 * @apiDescription Statistics of user contribution
	 * @apiName GetUserStatistics
	 * @apiGroup Users
	 * 
	 * @apiParam {String} uid The user ID
	 */
	app.route('/users/:uid/stats').get(users.stats);
	
	/**
	 * @api {get} /users/stats Users statistics
	 * @apiDescription Statistics of all users contributions
	 * @apiName GetUsersStatistics
	 * @apiGroup Users
	 */
	app.route('/users/stats').get(users.allStats);
	
	/**
	 * @api {get} /users/dataviz Instance statistics
	 * @apiDescription Statistics of usage of whole instance
	 * @apiName GetUsersDataviz
	 * @apiGroup Users
	 */
	app.route('/users/dataviz').get(users.dataviz);
	
	/**
	 * @api {get} /pictures/missing Missing pictures
	 * @apiDescription Places where pictures were not available
	 * @apiName GetPicturesMissing
	 * @apiGroup Pictures
	 */
	app.route('/pictures/missing').get(pictures.missing);
};
