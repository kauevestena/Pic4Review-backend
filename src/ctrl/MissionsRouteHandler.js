const Detections = require("../model/dataset/Detections");
const GeoJSON = require("../model/dataset/GeoJSON");
const jsts = require("jsts");
const Overpass = require("../model/dataset/Overpass");
const Osmose = require("../model/dataset/Osmose");
const PicturesStore = require("./PicturesStore");
const P4C = require("pic4carto");
const request = require("request");

const PAGE_LENGTH = 10;
const DATASETS = [ "osmose", "overpass", "detections" ];
const STATUSES = [ "online", "draft", "canceled", "deleted" ];
const MAX_FEATURES_PREVIEW = 100;
const MAX_FEATURES = 10000;
const LAYERS_URL = "https://osmlab.github.io/editor-layer-index/imagery.geojson";
const LAYERS_BLACKLIST = [
	'osmbe', 'osmfr', 'osm-mapnik-german_style', 'HDM_HOT', 'osm-mapnik-black_and_white', 'osm-mapnik-no_labels',
	'OpenStreetMap-turistautak', 'hike_n_bike', 'landsat', 'skobbler', 'public_transport_oepnv', 'tf-cycle',
	'tf-landscape', 'qa_no_address', 'wikimedia-map', 'openinframap-petroleum', 'openinframap-power', 'openinframap-telecoms',
	'openpt_map', 'openrailwaymap', 'openseamap', 'opensnowmap-overlay', 'US-TIGER-Roads-2012', 'US-TIGER-Roads-2014',
	'Waymarked_Trails-Cycling', 'Waymarked_Trails-Hiking', 'Waymarked_Trails-Horse_Riding', 'Waymarked_Trails-MTB',
	'Waymarked_Trails-Skating', 'Waymarked_Trails-Winter_Sports', 'OSM_Inspector-Addresses', 'OSM_Inspector-Geometry',
	'OSM_Inspector-Highways', 'OSM_Inspector-Multipolygon', 'OSM_Inspector-Places', 'OSM_Inspector-Routing', 'OSM_Inspector-Tagging'
];
let layers = [];

const picstore = new PicturesStore();

//Retrieve if available layers for editing
request(LAYERS_URL, (error, response, body) => {
	if(error) {
		console.error("[MissionsRouteHandler]", "Can't retrieve layers list", error);
	}
	else if(!response || response.statusCode !== 200) {
		console.error("[MissionsRouteHandler]", "Can't retrieve layers list", response && response.statusCode);
	}
	else {
		try {
			const data = JSON.parse(body);

			data.features.forEach(f => {
				try {
					const reader = new jsts.io.GeoJSONReader();
					const geom = f.geometry === null ? "everywhere" : reader.read(f.geometry);
					const props = f.properties;

					/*
					* Check imagery pertinence
					*/

					let valid = true;

					if(LAYERS_BLACKLIST.includes(props.id)) { valid = false; }
					if(![ "tms", "wms" ].includes(props.type)) { valid = false; }
					if(props.overlay) { valid = false; }

					if(props.end_date) {
						const endDate = new Date(props.end_date);

						if(!isNaN(endDate.getTime())) {
							const pastDate = new Date();
							pastDate.setFullYear(pastDate.getFullYear() - 10);
							if(endDate <= pastDate) { valid = false; }
						}
					}

					// Fix for IGN
					if(props.id == "fr.ign.bdortho") { props.max_zoom = 19; }

					if(valid) {
						layers.push({
							geometry: geom,
							properties: f.properties
						});
					}
				}
				catch(e) {
					console.error("[MissionsRouteHandler]", "Can't read layer "+f.properties.id, e.message);
				}
			});

			console.log("[MissionsRouteHandler]", "Loaded layers list");
		}
		catch(e) {
			console.error(e);
			console.error("[MissionsRouteHandler]", "Can't parse layers list");
		}
	}
});

/**
 * Route handler for missions-related routes.
 * @name MissionsRouteHandler
 */

/**
 * Handle /missions requests.
 */
exports.list = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.query) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(req.query.page && isNaN(parseInt(req.query.page))) {
		res.status(400).send({ "error": "Invalid page parameter, must be an integer starting to 1" });
	}
	else {
		const type = req.query.type || null;
		const theme = req.query.theme || null;
		const maxYears = req.query.maxYears || null; // NOT_SURE
		const page = parseInt(req.query.page) || 1;
		const editor = req.query.editor === "true";
		const status = req.query.status || "online";
		const complete = req.query.complete === "true";
		const user = req.query.user || null;
		const contributor = req.query.contributor || null;
		const sorts = { "pertinence": "score DESC", "newest": "created DESC", "oldest": "created", "least-complete": "completed", "most-complete": "completed DESC" };
		const orderBy = sorts[req.query.sort] || sorts.pertinence;
		const offset = (page-1) * PAGE_LENGTH;
		const params = [ PAGE_LENGTH, offset ];

		let conditions = [];

		if(!complete) {
			conditions.push("new > 0");
		}

		if(type) {
			conditions.push("type=$"+(params.length+1));
			params.push(type);
		}

		if(theme) {
			conditions.push("theme=$"+(params.length+1));
			params.push(theme);
		}

		if(maxYears) {
			conditions.push("maxYears=$"+(params.length+1)); // NOT SURE
			params.push(maxYears);
		}

		if(status && status !== "all") {
			conditions.push("status=$"+(params.length+1));
			params.push(status);
		}

		if(editor) {
			conditions.push("editor");
		}

		if(user) {
			conditions.push("userid=$"+(params.length+1));
			params.push(user);
		}

		if(contributor) {
			conditions.push("id IN (SELECT DISTINCT mission FROM edit WHERE userid = $"+(params.length+1)+")");
			params.push(contributor);
		}


		const request = "SELECT id, type, theme, maxYears, areaname, shortdesc, status, created, template, total::int, new::int, completed::int, seen::int, nopics::int, score, editor, illustration, contributors FROM mission_rank_cache "+(conditions.length > 0 ? "WHERE "+conditions.join(" AND ") : "")+" ORDER BY "+orderBy+" LIMIT $1 OFFSET $2";

		db.query(request, params)
		.then(d => {
			res.status(200).send({ missions: d.rows });
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve missions", "details_for_humans": "There was an error when looking for mission list, can you retry a bit later ?" });
		});
	}
};

/**
 * Handle /missions/map requests.
 */
exports.map = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.query) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else {
		const type = req.query.type || null;
		const theme = req.query.theme || null;
		const maxYears = req.query.maxYears || null; // NOT_SURE 
		const page = parseInt(req.query.page) || 1;
		const editor = req.query.editor === "true";
		const status = req.query.status || "online";
		const complete = req.query.complete === "true";
		const user = req.query.user || null;
		const contributor = req.query.contributor || null;
		const params = [];

		let conditions = [];

		if(!complete) {
			conditions.push("new > 0");
		}

		if(type) {
			conditions.push("type=$"+(params.length+1));
			params.push(type);
		}

		if(theme) {
			conditions.push("theme=$"+(params.length+1));
			params.push(theme);
		}

		if(status && status !== "all") {
			conditions.push("status=$"+(params.length+1));
			params.push(status);
		}

		if(editor) {
			conditions.push("editor");
		}

		if(user) {
			conditions.push("userid=$"+(params.length+1));
			params.push(user);
		}

		if(contributor) {
			conditions.push("id IN (SELECT DISTINCT mission FROM edit WHERE userid = $"+(params.length+1)+")");
			params.push(contributor);
		}


		const request = "SELECT id, type, theme, maxYears, areaname, shortdesc, status, created, total::int, new::int, completed::int, seen::int, nopics::int, score, editor, illustration, geom FROM mission_rank_cache "+(conditions.length > 0 ? "WHERE "+conditions.join(" AND ") : "")+" ORDER BY score DESC LIMIT 2000";

		db.query(request, params)
		.then(d => {
			//Transform into GeoJSON
			const result = {
				type: "FeatureCollection",
				features: []
			};

			d.rows.forEach(l => {
				const props = Object.assign({}, l);
				delete props.geom;

				result.features.push({
					type: "Feature",
					geometry: JSON.parse(l.geom),
					properties: props
				});
			});

			res.status(200).send({ geojson: result });
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve missions", "details_for_humans": "There was an error when looking for mission list, can you retry a bit later ?" });
		});
	}
};

/**
 * Handle /missions/templates requests.
 */
exports.templates = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else {
		// NOT_SURE: seems that here isn't needed, right?
		db.query("SELECT id, type, theme, shortdesc, fulldesc FROM mission WHERE template")
		.then(d => {
			res.status(200).send({ templates: d.rows });
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve templates", "details_for_humans": "There was an error when looking for template list, can you retry a bit later ?" });
		});
	}
};

/**
 * Handle /missions/:mid/stats/:uid requests.
 */
exports.levelup = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else if(!req.params.uid) {
		res.status(400).send({ "error": "Invalid user ID" });
	}
	else {
		//Check if mission exists
		db.query("SELECT * FROM mission WHERE id = $1", [ req.params.mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const result = {};
				const promises = [];

				//Amount of edits to level up
				promises.push(
					db.query("WITH a AS (SELECT place, cnt, mission FROM score_mission WHERE mission=$1 AND userid=$2) SELECT a.place, s.cnt - a.cnt + 1 AS next FROM score_mission s, a WHERE s.mission=a.mission AND s.place=a.place - 1", [ req.params.mid, req.params.uid ])
					.then(d => {
						if(d.rows.length === 1) {
							result.next = d.rows[0].next;
							result.place = d.rows[0].place;
						}
					})
				);

				//Amount of edits between current and previous one
				promises.push(
					db.query("WITH a AS (SELECT place, cnt, mission FROM score_mission WHERE mission=$1 AND userid=$2) SELECT a.place, a.cnt - s.cnt AS prev FROM score_mission s, a WHERE s.mission=a.mission AND s.place=a.place + 1", [ req.params.mid, req.params.uid ])
					.then(d => {
						if(d.rows.length === 1) {
							result.prev = d.rows[0].prev;
							result.place = d.rows[0].place;
						}
					})
				);

				Promise.all(promises)
				.then(() => {
					//Send results
					res.status(200).send(result);
				})
				.catch(e => {
					console.error(e);
					res.status(500).send({ "error": "Failed to retrieve mission statistics for this user", "details_for_humans": "I wasn't able to retrieve statistics for this mission, can you retry a bit later ?" });
				});
			}
			else {
				res.status(400).send({ "error": "Can't find given mission", "details_for_humans": "I can't find this mission, are you sure it's still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve given mission", "details_for_humans": "I can't find this mission, can you retry later ?" });
		});
	}
};

/**
 * Handle /missions/:mid requests.
 */
exports.details = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else {
		const query = req.query.synthetic === "1" ?
			"SELECT m.id, m.type, m.theme, m.maxYears, m.areaname, m.shortdesc, m.fulldesc, m.dataoptions->>'editors' AS editors, m.userid, ST_AsGeoJSON(m.geom) AS geom, m.created, m.status, mr.total::int, mr.new::int, mr.completed::int, mr.seen::int, mr.nopics::int, mr.illustration FROM mission m JOIN mission_rank_cache mr ON m.id = mr.id WHERE m.id=$1"
			: "SELECT m.id, m.type, m.theme, m.maxYears, m.areaname, m.shortdesc, m.fulldesc, m.datatype, m.dataoptions, m.userid, m.username, ST_AsGeoJSON(m.geom) AS geom, m.created, m.status, mr.total::int, mr.new::int, mr.completed::int, mr.seen::int, mr.nopics::int, mr.illustration FROM mission m JOIN mission_rank_cache mr ON m.id = mr.id WHERE m.id=$1";

		db.query(query, [ req.params.mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const result = d.rows[0];
				if(result.geom) { result.geom = JSON.parse(result.geom); }
				if(result.editors) {
					result.dataoptions = { editors: JSON.parse(result.editors) };
					delete result.editors;
				}

				//Find layers for editing
				result.layers = layers.filter(l => {
					if(l.geometry === "everywhere") {
						return true;
					}
					else {
						const g = (new jsts.io.GeoJSONReader()).read(result.geom);
						return l.geometry.contains(g.getCentroid());
					}
				})
				.map(l => l.properties);

				//Sort by pertinence
				result.layers.sort((a, b) => {
					if(a.best && b.best) {
						return 0;
					}
					else if(a.best) {
						return -1;
					}
					else if(b.best) {
						return 1;
					}
					else {
						if(a.default && b.default) {
							return 0;
						}
						else if(a.default) {
							return -1;
						}
						else {
							return 1;
						}
					}
				});

				//If user ID given, check if can edit
				if(req.query.userid) {
					//Author
					if(req.query.userid === result.userid) {
						res.status(200).send({ mission: result, canEdit: true });
					}
					//Or admin ?
					else {
						db.query("SELECT COUNT(*)::int AS cnt FROM admin WHERE userid = $1", [ req.query.userid ])
						.then(d => {
							res.status(200).send({ mission: result, canEdit: (d.rows.length === 1 && d.rows[0].cnt === 1) });
						})
						.catch(e => {
							console.error(e);
							res.status(500).send({ "error": "Can't check if user can edit mission", "details_for_humans": "I'm not sure you can edit this mission, can you retry a bit later ?" });
						});
					}
				}
				else {
					res.status(200).send({ mission: result });
				}
			}
			else {
				res.status(404).send({ "error": "No mission with given ID", "details_for_humans": "I can't find this mission, are you sure it is still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve mission", "details_for_humans": "Can't look for missions right now, can you retry a bit later ?" });
		});
	}
};

/**
 * Handle /missions/:mid/stats requests.
 */
exports.stats = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID" });
	}
	else {
		//Check if mission exists
		db.query("SELECT * FROM mission WHERE id = $1", [ req.params.mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const result = {};
				const promises = [];

				//Feature status summary
				promises.push(
					db.query("SELECT status, COUNT(*) AS cnt FROM feature WHERE mission = $1 GROUP BY status", [ req.params.mid ])
					.then(d => {
						result.status = {};

						d.rows.forEach(l => {
							result.status[l.status] = parseInt(l.cnt);
						});
					})
				);

				//Users statistics
				promises.push(
					db.query("SELECT username, cnt FROM score_mission WHERE mission = $1 ORDER BY place ASC", [ req.params.mid ])
					.then(d => {
						result.users = [];

						d.rows.forEach(l => {
							result.users.push({ user: l.username, featuresEdited: parseInt(l.cnt) });
						});
					})
				);

				//Month contributions
				promises.push(
					db.query("SELECT moment::date AS day, COUNT(*) AS cnt FROM edit WHERE mission = $1 AND age(moment) < '3 month' GROUP BY moment::date", [ req.params.mid ])
					.then(d => {
						result.days = {};

						d.rows.forEach(l => {
							result.days[l.day.toP4R()] = parseInt(l.cnt);
						});
					})
				);

				//Cumulated contributions
				promises.push(
					db.query("SELECT moment::date AS day, sum(COUNT(*)) OVER (ORDER BY moment::date) AS sumcontrib FROM edit WHERE mission = $1 AND prevstatus != 'reviewed' AND newstatus = 'reviewed' GROUP BY moment::date", [ req.params.mid ])
					.then(d => {
						result.contributions = {};

						d.rows.forEach(l => {
							result.contributions[l.day.toP4R()] = parseInt(l.sumcontrib);
						});
					})
				);

				Promise.all(promises)
				.then(() => {
					//Send results
					res.status(200).send(result);
				})
				.catch(e => {
					console.error(e);
					res.status(500).send({ "error": "Failed to retrieve mission statistics", "details_for_humans": "Can't load mission statistics for now, can you retry a bit later ?" });
				});
			}
			else {
				res.status(400).send({ "error": "Can't find given mission", "details_for_humans": "I can't find this mission, are you sure it is still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to retrieve given mission", "details_for_humans": "Can't look for missions right now, can you retry a bit later ?" });
		});
	}
};

const checkDataOptions = (datatype, dataoptions, params, res) => {
	if(!params.minlat || isNaN(parseFloat(params.minlat))) {
		res.status(400).send({ "error": "Invalid parameter minlat", "details_for_humans": "I can't figure where to create this mission, can you set the area for the mission ?" });
		return false;
	}
	else if(!params.maxlat || isNaN(parseFloat(params.maxlat))) {
		res.status(400).send({ "error": "Invalid parameter maxlat", "details_for_humans": "I can't figure where to create this mission, can you set the area for the mission ?" });
		return false;
	}
	else if(!params.minlon || isNaN(parseFloat(params.minlon))) {
		res.status(400).send({ "error": "Invalid parameter minlon", "details_for_humans": "I can't figure where to create this mission, can you set the area for the mission ?" });
		return false;
	}
	else if(!params.maxlon || isNaN(parseFloat(params.maxlon))) {
		res.status(400).send({ "error": "Invalid parameter maxlon", "details_for_humans": "I can't figure where to create this mission, can you set the area for the mission ?" });
		return false;
	}
	else {
		switch(datatype) {
			case "detections":
				if(!dataoptions.type || isNaN(parseInt(dataoptions.type)) || !P4C.Detection.TYPE_DETAILS[parseInt(dataoptions.type)]) {
					res.status(400).send({ "error": "Invalid parameter dataoptions.type", "details_for_humans": "Can you select a feature type for this mission ?" });
					return false;
				}
				break;

			case "osmose":
				if(!dataoptions.item || isNaN(parseInt(dataoptions.item))) {
					res.status(400).send({ "error": "Invalid parameter dataoptions.item", "details_for_humans": "Can you select an item for this Osmose mission ?" });
					return false;
				}
				else if(dataoptions.class && isNaN(parseInt(dataoptions.class))) {
					res.status(400).send({ "error": "Invalid parameter dataoptions.class", "details_for_humans": "The selected class for Osmose mission is not valid, maybe you can reload the page and retry ?" });
					return false;
				}
				else if(dataoptions.amount && isNaN(parseInt(dataoptions.amount))) {
					res.status(400).send({ "error": "Invalid parameter dataoptions.amount" , "details_for_humans": "I'm not sure how many objects I should retrieve for this Osmose mission, maybe you can reload the page ?" });
					return false;
				}
				break;

			case "overpass":
				if(!dataoptions.geojson || !dataoptions.geojson.features) {
					try {
						if(dataoptions.geojson && typeof dataoptions.geojson === "string") {
							const g = JSON.parse(dataoptions.geojson);
							if(!g.features) {
								throw new Error();
							}
						}
						else {
							throw new Error();
						}
					}
					catch(e) {
						res.status(400).send({ "error": "Invalid parameter dataoptions.geojson", "details_for_humans": "I don't have the list of objects to check, maybe there was an Overpass issue ? Can you retry a bit later ?" });
						return false;
					}
				}
				else if(dataoptions.geojson.features.length === 0) {
					res.status(400).send({ "error": "Empty dataoptions.geojson", "details_for_humans": "There are no features to review for this mission in this area. You can try another mission or this one elsewhere." });
					return false;
				}
				else if(dataoptions.oapi && dataoptions.oapi.length < 10) {
					res.status(400).send({ "error": "Invalid parameter dataoptions.oapi", "details_for_humans": "I don't have the Overpass API request, can you retry a bit later ?" });
					return false;
				}
				break;
		}
	}

	return true;
};

/**
 * Dataset generated from datatype + dataoptions
 */
const getDataset = (datatype, dataoptions) => {
	switch(datatype) {
		case "detections":
			return new Detections(parseInt(dataoptions.type), dataoptions);

		case "osmose":
			return new Osmose(dataoptions.item || dataoptions.itemId, parseInt(dataoptions.amount) || null, dataoptions);

		case "overpass":
			if(dataoptions.geojson) { return new GeoJSON(dataoptions.geojson); }
			else if(dataoptions.query) { return new Overpass(dataoptions.query, dataoptions); }

		default:
			return null;
	}
};

/**
 * Returns several promises for adding features in bulk
 */
const insertFeaturesIntoDb = (mid, features) => {
	const ftPromises = [];
	const writer = new jsts.io.WKTWriter();

	for(let i=0; i < features.length; i+=100) {
		/*
		 * Insert features into DB
		 */
		const featuresPart = features.slice(i, i+100);

		let request = "INSERT INTO feature(mission, status, properties, geom, geomfull, sourceid) VALUES ";
		const params = [];
		const values = [];
		const maxv = 7;

		featuresPart.forEach((f, i) => {
			params.push("($"+(i*maxv+1)+", $"+(i*maxv+2)+", $"+(i*maxv+3)+", ST_SetSRID(ST_Point($"+(i*maxv+4)+", $"+(i*maxv+5)+"), 4326), ST_SetSRID(ST_GeomFromText($"+(i*maxv+6)+"), 4326), $"+(i*maxv+7)+")");
			values.push(mid);
			values.push('new');
			values.push(JSON.stringify(f.properties));
			values.push(f.coordinates[1]);
			values.push(f.coordinates[0]);
			values.push(writer.write(f.geom));
			values.push(f.id);
		});

		request += params.join(", ");

		ftPromises.push(db.query(request, values));
	}

	return ftPromises;
};

/**
 * Handle POST /missions requests.
 */
exports.create = function(req, res) {
	/*
	 * Check mandatory parameters
	 */
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.body) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.body.type || req.body.type.length < 3) {
		res.status(400).send({ "error": "Invalid parameter type", "details_for_humans": "Can you select a type of mission please ?" });
	}
	else if(!req.body.theme || req.body.theme.length < 3) {
		res.status(400).send({ "error": "Invalid parameter theme", "details_for_humans": "Can you select a theme for this mission please ?" });
	}

	// NOT_SURE: hope it's well described
	else if(!req.body.maxYears || req.body.maxYears < 1) {
		res.status(400).send({ "error": "Invalid parameter number of Years", "details_for_humans": "Can set the max years as a positive number?" });
	}
	else if(!req.body.areaname || req.body.areaname.length < 3) {
		res.status(400).send({ "error": "Invalid parameter areaname", "details_for_humans": "Can you give a not-too-short name for the mission area ?" });
	}
	else if(!req.body.shortdesc || req.body.shortdesc.length < 10) {
		res.status(400).send({ "error": "Invalid parameter shortdesc", "details_for_humans": "Can you give a mission name, which is not too short ?" });
	}
	else if(!req.body.fulldesc || req.body.fulldesc.length < 50) {
		res.status(400).send({ "error": "Invalid parameter fulldesc", "details_for_humans": "Can you give more details about this mission ? This should be clear enough for new contributors" });
	}
	else if(!req.body.datatype || DATASETS.indexOf(req.body.datatype) < 0) {
		res.status(400).send({ "error": "Invalid parameter datatype", "details_for_humans": "I'm not sure which kind of mission I should create, can you retry a bit later ?" });
	}
	else if(!req.body.dataoptions) {
		res.status(400).send({ "error": "Invalid parameter dataoptions", "details_for_humans": "I'm missing some details to create the mission, can you retry a bit later ?" });
	}
	else if(!req.body.username || !req.body.userid) {
		res.status(400).send({ "error": "Invalid user info", "details_for_humans": "I'm not sure if you're logged in, can you retry a bit later ?" });
	}
	else {
		const dataoptions = req.body.dataoptions;

		//Check dataoptions
		if(checkDataOptions(req.body.datatype, dataoptions, req.body, res)) {
			const [ lat1, lat2, lng1, lng2 ] = [ req.body.minlat, req.body.maxlat, req.body.minlon, req.body.maxlon ];
			const bbox = new P4C.LatLngBounds(new P4C.LatLng(lat1, lng1), new P4C.LatLng(lat2, lng2));
			const linestring = 'LINESTRING('+lng1+' '+lat1+', '+lng2+' '+lat1+', '+lng2+' '+lat2+', '+lng1+' '+lat2+', '+lng1+' '+lat1+')';

			// Find illustration for mission if possible
			let illustration = null;
			if(req.body.datatype === "detections") {
				illustration = P4C.Detection.TYPE_DETAILS[parseInt(dataoptions.type)].symbol;
			}
			else if(req.body.datatype === "osmose") {
				illustration = Osmose.ILLUSTRATIONS[dataoptions.item];
			}

			dataoptions.bbox = bbox;
			const dataset = getDataset(req.body.datatype, dataoptions);

			/*
			 * Load features from dataset
			 */

			if(dataset) {
				dataset.getAllFeatures()
				.then(features => {
					//Check received features
					if(features && features.length > 0 && features.length <= MAX_FEATURES) {

						/*
						 * Create mission
						 */

						db.query(
							"INSERT INTO mission(type, theme,maxYears, areaname, shortdesc, fulldesc, datatype, dataoptions, userid, username, illustration, geom) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, ST_SetSRID(ST_MakePolygon($11), 4326)) RETURNING id",
							[
								req.body.type,
								req.body.theme,
								req.body.maxYears,
								req.body.areaname,
								req.body.shortdesc,
								req.body.fulldesc,
								req.body.datatype,
								dataoptions,
								req.body.userid,
								req.body.username,
								illustration,
								linestring
							]
						).then(d => {
							if(d.rows.length === 1) {
								const mid = d.rows[0].id;

								// Insert features into DB
								Promise.all(insertFeaturesIntoDb(mid, features))
								.then(() => {
									const prep = picstore.preparePicturesForMission(mid, true,req.body.maxYears); // NOT_SURE if it's like that, neither if is the best way or even will work 
									res.status(200).send({ "info": "Features being prepared for this mission", "id": mid, "pictoken": prep.token });
								})
								.catch(e => {
									console.error(e);
									res.status(500).send({ "error": "Failed to import features for this mission", "id": mid, "details_for_humans": "I can't retrieve the feature for this mission, you might retry later" });
								});
							}
							else {
								res.status(500).send({ "error": "Failed to get mission ID", "details_for_humans": "Something broke when creating the mission, can you retry a bit later ?" });
							}
						})
						.catch(e => {
							console.error(e);
							res.status(500).send({ "error": "Failed to create mission", "details_for_humans": "Something broke when creating the mission, can you retry a bit later ?" });
						});
					}
					else {
						if(!features || features.length === 0) {
							res.status(500).send({ "error": "No features for this mission", "details_for_humans": "It seems there is no features to review for this mission. You can try to create it somewhere else ?" });
						}
						else {
							res.status(500).send({ "error": "Too many features for this mission", "details_for_humans": "This mission has too many features to review. Please retry by selecting a smaller area." });
						}
					}
				})
				.catch(e => {
					console.error(e);
					res.status(500).send({ "error": "Can't get features for this dataset", "details_for_humans": "Something broke when loading features for this mission, can you try a bit later ?" });
				});
			}
			else {
				res.status(500).send({ "error": "Failed to create dataset", "details_for_humans": "Something broke when creating the mission, can you retry a bit later ?" });
			}
		}
	}
};

/**
 * Updates feature of the given mission according to datasource
 * @param {string} mid The mission ID
 * @return {Promise} Resolves when features are ready
 */
exports.updateFeatures = function(mid) {
	return db.query("SELECT datatype, dataoptions, ST_XMin(geom) AS xmin, ST_XMax(geom) AS xmax, ST_YMin(geom) AS ymin, ST_YMax(geom) AS ymax FROM mission WHERE id = $1", [ mid ])
	.then(d => {
		if(d.rows.length === 1) {
			const datatype = d.rows[0].datatype;
			const dataoptions = d.rows[0].dataoptions;

			if(dataoptions.geojson) { delete dataoptions.geojson; }
			dataoptions.bbox = new P4C.LatLngBounds(new P4C.LatLng(d.rows[0].ymin, d.rows[0].xmin), new P4C.LatLng(d.rows[0].ymax, d.rows[0].xmax));

			const dataset = getDataset(datatype, dataoptions);

			if(dataset) {
				// Fetch features from data source
				return dataset.getAllFeatures()
				.then(dsFeatures => {
					if(!dsFeatures || dsFeatures.length === 0) {
						return false;
					}

					// Index features by ID
					const dsFeaturesById = {};

					dsFeatures.forEach(f => {
						if(f.id) {
							dsFeaturesById[f.id] = f;
						}
					});

					// Fetch features from database
					return db.query("SELECT id, sourceid, properties, status, geom FROM feature WHERE mission = $1", [ mid ])
					.then(d2 => {
						// Index features by ID
						const dbFeaturesById = {};

						d2.rows.forEach(r => {
							const fid = r.sourceid || r.id;
							if(fid) {
								dbFeaturesById[fid] = r;
							}
						});

						const dbKeys = Object.keys(dbFeaturesById);
						const dsKeys = Object.keys(dsFeaturesById);

						// Put features into identified groups
						const notAvailableAnymore = dbKeys.filter(k => dsKeys.indexOf(k) < 0);
						const newFromSource = dsKeys.filter(k => dbKeys.indexOf(k) < 0);
						const toUpdate = dbKeys.filter(k => {
							if(dbFeaturesById[k].status === 'reviewed') { return false; }
							else if(JSON.stringify(dbFeaturesById[k].properties) === JSON.stringify(dsFeaturesById[k])) { return false; }
							else { return true; }
						});

						const promises = [];

						// Mark as reviewed features which are not available anymore from data source
						if(notAvailableAnymore.length > 0) {
							const notAvailableAnymoreIds = notAvailableAnymore.map(k => dbFeaturesById[k].id);
							promises.push(db.query("UPDATE feature SET status = 'reviewed' WHERE mission = $1 AND id IN ("+ notAvailableAnymoreIds.join(", ") +")", [ mid ]));
						}

						// Insert new features from data source
						if(newFromSource.length > 0) {
							const newFeatures = newFromSource.map(k => dsFeaturesById[k]);
							insertFeaturesIntoDb(mid, newFeatures).forEach(p => promises.push(p));
						}

						// Update existing features not reviewed which differ in data source
						if(toUpdate.length > 0) {
							const tuples = toUpdate
							.map(tu => {
								const f = dsFeaturesById[tu];
								return f ? [
									dbFeaturesById[tu].id,
									f.properties,
									f.coordinates[1],
									f.coordinates[0]
								] : null;
							})
							.filter(t => t !== null);

							promises.push(db.query(
								"UPDATE feature "+
								"SET properties = b.props, geom = ST_SetSRID(ST_Point(b.x, b.y), 4326) "+
								"FROM ("+
								"SELECT (value->>0)::int AS id, (value->>1)::json AS props, (value->>2)::decimal AS x, (value->>3)::decimal AS y "+
								"FROM json_array_elements($2)"+
								") b "+
								"WHERE feature.mission = $1 AND feature.id = b.id",
								[
									mid,
									JSON.stringify(tuples)
								]
							));
						}

						return Promise.all(promises)
						.then(() => {
							return db.query("UPDATE mission SET lastedit = current_timestamp WHERE id = $1", [ mid ]);
						});
					});
				})
				.catch(e => {
					console.error(e);
					return false;
				});
			}
			else {
				return db.query("UPDATE mission SET lastedit = current_timestamp WHERE id = $1", [ mid ]);
			}
		}
		else {
			return false;
		}
	});
};

/**
 * Handle GET /missions/loading requests.
 */
exports.loading = function(req, res) {
	if(!req || !req.query) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(isNaN(parseInt(req.query.pictoken))) {
		res.status(400).send({ "error": "Invalid pictoken parameter", "details_for_humans": "The mission isn't yet ready for loading status, please wait" });
	}
	else {
		res.status(200).send({ "loading": picstore.getRetrievalStatus(parseInt(req.query.pictoken)) });
	}
};

/**
 * Handle PUT /missions/:mid requests.
 */
exports.update = function(req, res) {
	//Check mandatory parameters
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.params) {
		res.status(400).send({ "error": "Missing parameters" });
	}
	else if(!req.params.mid || isNaN(parseInt(req.params.mid))) {
		res.status(400).send({ "error": "Invalid mission ID", "details_for_humans": "I can't find this mission, are you sure it is still online ?" });
	}
	else if(!req.body) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.body.userid) {
		res.status(400).send({ "error": "Invalid user info", "details_for_humans": "I'm not sure if you're logged in, can you retry a bit later ?" });
	}
	else if(req.body.status && STATUSES.indexOf(req.body.status) < 0) {
		res.status(400).send({ "error": "Invalid mission status" });
	}
	else if(req.body.type && req.body.type.length < 3) {
		res.status(400).send({ "error": "Invalid parameter type", "details_for_humans": "Can you select a type of mission please ?" });
	}
	else if(req.body.theme && req.body.theme.length < 3) {
		res.status(400).send({ "error": "Invalid parameter theme", "details_for_humans": "Can you select a theme for this mission please ?" });
	}

	// NOT_SURE: hope it's well described
	else if(!req.body.maxYears || req.body.maxYears < 1) {
		res.status(400).send({ "error": "Invalid parameter number of Years", "details_for_humans": "Can set the max years as a positive number?" });
	}

	else if(req.body.areaname && req.body.areaname.length < 3) {
		res.status(400).send({ "error": "Invalid parameter areaname", "details_for_humans": "Can you give a not-too-short name for the mission area ?" });
	}
	else if(req.body.shortdesc && req.body.shortdesc.length < 10) {
		res.status(400).send({ "error": "Invalid parameter shortdesc", "details_for_humans": "Can you give a mission name, which is not too short ?" });
	}
	else if(req.body.fulldesc && req.body.fulldesc.length < 50) {
		res.status(400).send({ "error": "Invalid parameter fulldesc", "details_for_humans": "Can you give more details about this mission ? This should be clear enough for new contributors" });
	}
	else if(req.body.dataoptions && typeof req.body.dataoptions !== "object") {
		res.status(400).send({ "error": "Invalid parameter dataoptions", "details_for_humans": "I'm missing some details to create the mission, can you retry a bit later ?" });
	}
	else if(req.body.template && req.body.template !== true && req.body.template !== false) {
		res.status(400).send({ "error": "Invalid parameter template", "details_for_humans": "Can you give me a template value which is either true or false ?" });
	}
	else {
		//Check if mission exists
		db.query("SELECT id, userid, status FROM mission WHERE id = $1", [ req.params.mid ])
		.then(d => {
			if(d.rows.length === 1) {
				const m = d.rows[0];
				let authorization = null;
				let isAdmin = false;

				//Check user account
				db.query("SELECT COUNT(*)::int AS cnt FROM admin WHERE userid = $1", [ req.body.userid ])
				.then(d => {
					if(d.rows.length === 1 && d.rows[0].cnt === 1) {
						return true;
					}
					else {
						if(m.userid === req.body.userid.toString()) {
							return false;
						}
						else {
							throw new Error("Unauthorized");
						}
					}
				})
				.then(isAdmin => {
					const fields = [];
					const params = [ req.params.mid ];
					const available = [ "status", "type", "theme","maxYears", "areaname", "shortdesc", "fulldesc", "dataoptions" ];
					if(isAdmin) { available.push("template"); }

					available.forEach(a => {
						if(req.body[a] != null) {
							fields.push(a);
							params.push(req.body[a]);
						}
					});

					if(fields.length > 0) {
						const entries = fields.map((f, i) => f+" = $"+(i+2)).join(", ");

						//Update
						db.query("UPDATE mission SET lastedit = current_timestamp, "+entries+" WHERE id = $1", params)
						.then(() => {
							res.status(200).send({ "info": "Mission updated" });
						})
						.catch(e => {
							console.error(e);
							res.status(500).send({ "error": "Failed to update mission", "details_for_humans": "Something broke when updating the mission, can you retry later ?" });
						});
					}
					else {
						res.status(400).send({ "error": "No detail to update given", "details_for_humans": "There was nos changes to mission details, can you check what you set ?" });
					}
				})
				.catch(e => {
					res.status(403).send({ "error": "You are not authorized to edit this mission", "details_for_humans": "Hmm, it seems you are not authorized to edit this mission. Are you sure to be logged-in ?" });
				});
			}
			else {
				res.status(400).send({ "error": "Invalid mission ID", "details_for_humans": "I can't find this mission, are you sure it is still online ?" });
			}
		})
		.catch(e => {
			console.error(e);
			res.status(500).send({ "error": "Failed to find mission", "details_for_humans": "Something broke when looking for the mission, can you retry a bit later ?" });
		});
	}
};

/**
 * Handle /missions/preview route
 */
exports.preview = function(req, res) {
	/*
	 * Check mandatory parameters
	 */
	if(!req) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.body) {
		res.status(400).send({ "error": "Invalid request" });
	}
	else if(!req.body.datatype || DATASETS.indexOf(req.body.datatype) < 0) {
		res.status(400).send({ "error": "Invalid parameter datatype", "details_for_humans": "I'm not sure what kind of mission you want to preview, can you select one data source ?" });
	}
	else if(!req.body.dataoptions) {
		res.status(400).send({ "error": "Invalid parameter dataoptions", "details_for_humans": "I'm lacking some info for previewing the mission, can you fill the details for the data source ?" });
	}
	else {
		const dataoptions = req.body.dataoptions;
		const [ lat1, lat2, lng1, lng2 ] = [ req.body.minlat, req.body.maxlat, req.body.minlon, req.body.maxlon ];
		let dataset = null;

		//Check dataoptions
		if(checkDataOptions(req.body.datatype, dataoptions, req.body, res)) {
			//Find features
			if(req.body.datatype === "detections") {
				dataoptions.bbox = new P4C.LatLngBounds(new P4C.LatLng(lat1, lng1), new P4C.LatLng(lat2, lng2));

				dataset = new Detections(parseInt(dataoptions.type), dataoptions);
			}
			else if(req.body.datatype === "osmose") {
				dataoptions.bbox = new P4C.LatLngBounds(new P4C.LatLng(lat1, lng1), new P4C.LatLng(lat2, lng2));

				dataset = new Osmose(dataoptions.item, parseInt(dataoptions.amount) || null, dataoptions);
			}
			else if(req.body.datatype === "overpass") {
				try {
					dataset = new GeoJSON(dataoptions.geojson);
				}
				catch(e) {
					console.error(e);
					res.status(500).send({ "error": "Can't create Overpass dataset", "details_for_humans": "Something broke when trying to create preview for this Overpass mission, can you retry a bit later ?" });
				}
			}

			dataset.getAllFeatures()
			.then(features => {
				//Check received features
				if(features && features.length > 0) {
					const fts = features.length > MAX_FEATURES_PREVIEW ? features.slice(0, MAX_FEATURES_PREVIEW) : features;

					//Find pictures for each feature
					const prep = picstore.getPicturesForFeatures(fts); //  NOT SURE on how to put maxYears there... then tried a trick to set at preparePicturesForMission within it's call, because it seems to be called before this one...
				
					prep.ready.then(f => {
						res.status(200).send({ "features": f, "limited": features.length > MAX_FEATURES_PREVIEW });
					})
					.catch(e => {
						console.error(e);
						res.status(500).send({ "error": "Can't get preview for given features", "details_for_humans": "Something broke when loading pictures for this preview, can you try a bit later ?" });
					});
				}
				else {
					res.status(200).send({ "features": [] });
				}
			})
			.catch(e => {
				console.error(e);
				res.status(500).send({ "error": "Can't get features for this dataset", "details_for_humans": "Something broke when loading features for this preview, can you try a bit later ?" });
			});
		}
	}
};
