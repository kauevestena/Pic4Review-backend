--------------------------
--                      --
-- Database init script --
--                      --
--------------------------

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM pg_database WHERE datname = 'p4r_db') THEN
        CREATE DATABASE p4r_db;
    END IF;
END $$;

-- Extensions
CREATE EXTENSION IF NOT EXISTS postgis;


-- Missions
CREATE TABLE IF NOT EXISTS mission(
	id BIGSERIAL PRIMARY KEY,
	type VARCHAR(50) NOT NULL,
	theme VARCHAR(50) NOT NULL,
	maxYears INT NOT NULL DEFAULT 3,
	areaname VARCHAR(100),
	shortdesc VARCHAR(100) NOT NULL,
	fulldesc TEXT,
	datatype VARCHAR(50) NOT NULL,
	dataoptions JSON,
	userid BIGSERIAL NOT NULL,
	username VARCHAR(100) NOT NULL,
	status VARCHAR(20) NOT NULL DEFAULT 'draft',
	lastedit TIMESTAMP NOT NULL DEFAULT current_timestamp,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp,
	template BOOLEAN NOT NULL DEFAULT false,
	illustration TEXT,
	geom GEOMETRY(POLYGON, 4326) NOT NULL
);

CREATE INDEX IF NOT EXISTS mission_id_idx ON mission(id);
CREATE INDEX IF NOT EXISTS mission_geom_idx ON mission USING GIST(geom);
CREATE INDEX IF NOT EXISTS mission_template_idx ON mission(template);


-- Features
CREATE TABLE IF NOT EXISTS feature(
	id BIGSERIAL PRIMARY KEY,
	mission BIGINT NOT NULL,
	properties JSON,
	status VARCHAR(20) NOT NULL DEFAULT 'new',
	lastedit TIMESTAMP NOT NULL DEFAULT current_timestamp,
	geom GEOMETRY(POINT, 4326) NOT NULL,
	geomfull GEOMETRY(GEOMETRY, 4326) NOT NULL,
	pictures JSON,
	lastpicedit TIMESTAMP NOT NULL DEFAULT '1970-01-01',
	sourceid VARCHAR,
	FOREIGN KEY (mission) REFERENCES mission(id)
);

CREATE INDEX IF NOT EXISTS feature_id_idx ON feature(id);
CREATE INDEX IF NOT EXISTS feature_geom_idx ON feature USING GIST(geom);
CREATE INDEX IF NOT EXISTS feature_geomfull_idx ON feature USING GIST(geomfull);
CREATE INDEX IF NOT EXISTS feature_mission_idx ON feature(mission);
CREATE INDEX IF NOT EXISTS feature_status_idx ON feature(status);


-- Edits
CREATE TABLE IF NOT EXISTS edit(
	id BIGSERIAL PRIMARY KEY,
	userid BIGSERIAL NOT NULL,
	username VARCHAR(100) NOT NULL,
	moment TIMESTAMP NOT NULL DEFAULT current_timestamp,
	prevstatus VARCHAR(20) NOT NULL,
	newstatus VARCHAR(20) NOT NULL,
	mission BIGINT NOT NULL,
	feature BIGINT NOT NULL,
	FOREIGN KEY (mission) REFERENCES mission(id),
	FOREIGN KEY (feature) REFERENCES feature(id)
);

CREATE INDEX IF NOT EXISTS edit_id_idx ON edit(id);
CREATE INDEX IF NOT EXISTS edit_when_idx ON edit(moment);
CREATE INDEX IF NOT EXISTS edit_mission_idx ON edit(mission);
CREATE INDEX IF NOT EXISTS edit_userid_idx ON edit(userid);


-- Features skips
CREATE TABLE IF NOT EXISTS feature_skip(
	id BIGSERIAL PRIMARY KEY,
	userid BIGSERIAL NOT NULL,
	username VARCHAR(100) NOT NULL,
	moment TIMESTAMP NOT NULL DEFAULT current_timestamp,
	mission BIGINT NOT NULL,
	feature BIGINT NOT NULL,
	FOREIGN KEY (mission) REFERENCES mission(id),
	FOREIGN KEY (feature) REFERENCES feature(id)
);

CREATE INDEX IF NOT EXISTS feature_skip_id_idx ON feature_skip(id);
CREATE INDEX IF NOT EXISTS feature_skip_when_idx ON feature_skip(moment);

-- Update feature being too much skipped
CREATE OR REPLACE FUNCTION updateSkippedFeatures() RETURNS TRIGGER AS $$
BEGIN
	UPDATE feature SET status = 'cantsee'
	WHERE status = 'new' AND id IN (
		SELECT feature
		FROM feature_skip
		GROUP BY feature
		HAVING COUNT(*) >= 3
	);
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS feature_skipping ON feature_skip;
CREATE TRIGGER feature_skipping
AFTER INSERT ON feature_skip
EXECUTE PROCEDURE updateSkippedFeatures();


-- Queries
CREATE TABLE IF NOT EXISTS http_queries(
	id BIGSERIAL PRIMARY KEY,
	ts TIMESTAMP DEFAULT current_timestamp,
	userid VARCHAR(150),
	ip VARCHAR(100),
	path VARCHAR(200),
	query JSON
);


-- User names
CREATE OR REPLACE VIEW username AS
SELECT
	a.userid,
	e.username
FROM (
	SELECT userid, MAX(moment) AS last
	FROM edit
	GROUP BY userid
) a
JOIN edit e
	ON a.userid = e.userid
	AND a.last = e.moment;


-- User scores
DROP VIEW IF EXISTS score;
CREATE VIEW score AS
SELECT
	a.*,
	u.username
FROM (
	SELECT
		userid,
		COUNT(*) AS cnt,
		row_number() OVER(ORDER BY COUNT(*) DESC) AS place
	FROM edit
	WHERE age(moment) <= '1 month'
	GROUP BY userid
) a
JOIN username u ON a.userid = u.userid;


-- Administrators
CREATE TABLE IF NOT EXISTS admin(
	userid BIGINT PRIMARY KEY,
	username VARCHAR
);


-- Mission completion + ranking
DROP VIEW IF EXISTS mission_rank;
CREATE VIEW mission_rank AS
SELECT
	*,
	(35 * least(100, contributors) + 30 * least(100, 100-completed) + 20 * least(100, recent) + 15 * editorscore) / 100 AS score
FROM (
	SELECT
		*,
		CASE WHEN seen + new > 0 THEN FLOOR(seen::float / (seen+new)::float * 100) ELSE 100 END AS completed,
		100 - (current_date - created::date)::float / (current_date - '2017-12-01'::date)::float * 100 AS recent,
		CASE WHEN editor THEN 100 ELSE 0 END AS editorscore
	FROM (
		SELECT
			m.*,
			CASE WHEN c.contributors IS NOT NULL THEN c.contributors ELSE 0 END AS contributors,
			COALESCE(t.total, 0) AS total,
			COALESCE(t.new, 0) AS new,
			COALESCE(t.nopics, 0) AS nopics,
			COALESCE(t.seen, 0) AS seen
		FROM (
			SELECT
				id, type, theme, maxYears, areaname, shortdesc, lastedit, status, created, userid, username, template,
				(dataoptions->>'editors' is not null AND json_extract_path_text(dataoptions, 'editors', 'type') != 'disabled') AS editor,
				CASE
					WHEN illustration IS NOT NULL
						THEN illustration
					WHEN dataoptions->>'editors' is not null AND json_extract_path_text(dataoptions, 'editors', 'type') = 'images'
						THEN (json_extract_path(dataoptions, 'editors', 'answers')->>0)::json->>'image'
				END AS illustration,
				ST_AsGeoJSON(ST_Centroid(geom)) AS geom
			FROM mission
			WHERE status != 'deleted'
		) m
		LEFT JOIN (
			SELECT mission,
				COUNT(*) AS total,
				SUM(CASE WHEN status = 'nopics' THEN 1 ELSE 0 END) AS nopics,
				SUM(CASE WHEN status = 'new' THEN 1 ELSE 0 END) AS new,
				SUM(CASE WHEN status NOT IN ('nopics', 'new') THEN 1 ELSE 0 END) AS seen
			FROM feature
			GROUP BY mission
		) t ON m.id = t.mission
		LEFT JOIN (
			SELECT mission, count(distinct userid) AS contributors
			FROM edit
			GROUP BY mission
		) c ON m.id = c.mission
	) a
) a;

CREATE OR REPLACE FUNCTION updateMissionRankCache() RETURNS VOID AS $$
BEGIN
	DROP TABLE IF EXISTS mission_rank_cache;
	
	CREATE TABLE mission_rank_cache AS
	SELECT *
	FROM mission_rank;

	CREATE INDEX mission_rank_cache_new_idx ON mission_rank_cache(new);
	CREATE INDEX mission_rank_cache_type_idx ON mission_rank_cache(type);
	CREATE INDEX mission_rank_cache_theme_idx ON mission_rank_cache(theme);
	CREATE INDEX mission_rank_cache_maxYears_idx ON mission_rank_cache(maxYears);
	CREATE INDEX mission_rank_cache_status_idx ON mission_rank_cache(status);
	CREATE INDEX mission_rank_cache_editor_idx ON mission_rank_cache(editor);
	CREATE INDEX mission_rank_cache_userid_idx ON mission_rank_cache(userid);
	CREATE INDEX mission_rank_cache_score_idx ON mission_rank_cache(score);
END;
$$ LANGUAGE plpgsql;

SELECT updateMissionRankCache();

CREATE OR REPLACE FUNCTION updateMissionRankCacheTgr() RETURNS TRIGGER AS $$
BEGIN
	PERFORM updateMissionRankCache();
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS mission_rank_cache_update ON mission;
CREATE TRIGGER mission_rank_cache_update
AFTER UPDATE ON mission
EXECUTE PROCEDURE updateMissionRankCacheTgr();


-- User score by mission
DROP VIEW IF EXISTS score_mission;
CREATE VIEW score_mission AS
SELECT
	a.*,
	u.username
FROM (
	SELECT
		mission,
		userid,
		COUNT(*) AS cnt,
		row_number() OVER(PARTITION BY mission ORDER BY COUNT(*) DESC) AS place
	FROM edit
	WHERE age(moment) <= '1 month'
	GROUP BY mission, userid
) a
JOIN username u ON a.userid = u.userid;


-- Pictures (deprecated)
DROP TABLE IF EXISTS picture;
DROP VIEW IF EXISTS feature_picture;
DROP FUNCTION IF EXISTS updateFeaturePictureCache();
DROP TRIGGER IF EXISTS picture_cleaning ON mission;
DROP FUNCTION IF EXISTS cleanPictureTable();


-- LngLat to Envelope
DROP FUNCTION IF EXISTS coordsToBbox();


-- Outdated missions
DROP VIEW IF EXISTS mission_outdated;
CREATE VIEW mission_outdated AS
SELECT id
FROM mission
WHERE
	status = 'online'
	AND age(timezone('utc', now()), lastedit) > interval '1 hour'
ORDER BY lastedit ASC;



-- Pictures providers
DROP VIEW IF EXISTS picture_provider;
CREATE VIEW picture_provider AS
SELECT
	json_array_elements(pictures)->>'provider' AS provider,
	count(*) AS cnt
FROM feature
WHERE pictures IS NOT NULL
GROUP BY provider;

CREATE OR REPLACE FUNCTION updatePictureProviderCache() RETURNS VOID AS $$
BEGIN
	DROP TABLE IF EXISTS picture_provider_cache;
	
	CREATE TABLE picture_provider_cache AS
	SELECT *
	FROM picture_provider;
END;
$$ LANGUAGE plpgsql;

SELECT updatePictureProviderCache();



-- List inactive missions
DROP VIEW IF EXISTS mission_to_disable;
CREATE VIEW mission_to_disable AS
SELECT id
FROM (
	SELECT m.id, AGE(MAX(e.moment)) AS age
	FROM mission m
	LEFT JOIN edit e ON m.id = e.mission
	WHERE m.status = 'online'
	GROUP BY m.id
	HAVING AGE(MAX(e.moment)) >= '30 days'
) a
UNION
SELECT id
FROM (
	SELECT m.id, m.created, AGE(MAX(e.moment)) AS age
	FROM mission m
	LEFT JOIN edit e ON m.id = e.mission
	WHERE m.status = 'online' AND AGE(m.created) >= '15 days'
	GROUP BY m.id, m.created
	HAVING AGE(MAX(e.moment)) IS NULL
) b;
