/**
 * Test class for PicturesStore
 */

const assert = require('assert');
const DbManager = require('../../src/ctrl/DbManager');
const GeoJSON = require('../../src/model/dataset/GeoJSON');
const PicStore = require("../../src/ctrl/PicturesStore");

const CONFIG = require("../../config.json");
const DB_HOST = CONFIG.db.host;
const DB_USER = CONFIG.db.user;
const DB_PASS = CONFIG.db.pass;
const DB_NAME = CONFIG.db.name;

const TIMEOUT = 10000;

const db = new DbManager(DB_HOST, DB_USER, DB_PASS, DB_NAME);
db.initDb()
.then(() => {
	global.db = db;
});

const [ lat1, lat2, lat3, lat4, lng1, lng2, lng3, lng4 ] = [ 48.0769155, 48.1549705, 48.12790, 49.7734, -1.7525876, -1.6244045, -1.68330, -31.9854 ];
const gj = {"type":"FeatureCollection","features":[
	{"type":"Feature","properties":{"id": 1, "k1":"v1"},"geometry":{"type":"Point","coordinates":[lng3, lat3]}},
	{"type":"Feature","properties":{"id": 2, "k1":"v1"},"geometry":{"type":"Point","coordinates":[lng4, lat4]}},
]};

const missionParams = [
	"fix",
	"amenity",
	"Rennes, France",
	"Toilettes invalides",
	"Toilettes invalides à Rennes",
	"overpass",
	{ query: "", geojson: gj },
	"1234",
	"PanierAvide",
	'LINESTRING('+lng1+' '+lat1+', '+lng2+' '+lat1+', '+lng2+' '+lat2+', '+lng1+' '+lat2+', '+lng1+' '+lat1+')'
];

describe("Ctrl > PicturesStore", () => {
	beforeEach(done => {
		if(!global.db) {
			const timer = setInterval(() => {
				if(global.db) {
					clearInterval(timer);
					done();
				}
			});
		}
		else {
			done();
		}
	});
	
	describe("Constructor", () => {
		it("works", () => {
			const p1 = new PicStore();
			assert.equal(p1.statuses.length, 0);
		});
	});
	
	describe("getPicturesForFeatures", () => {
		const dataset = new GeoJSON(gj);
		
		it("retrieves pictures correctly", done => {
			const p1 = new PicStore();
			
			//Get features
			dataset.getAllFeatures()
			.then(features => {
				
				//Retrieve pictures
				const res = p1.getPicturesForFeatures(features);
				assert.equal(p1.getRetrievalStatus(res.token), 0);
				
				res.ready.then(fts => {
					
					//Check
					assert.equal(fts.length, 2);
					fts.sort((a, b) => parseInt(a.id) - parseInt(b.id));
					
					const f = fts[0];
					assert.equal(f.status, 'new');
					assert.ok(f.pictures.length > 0);
					assert.ok(f.id != null);
					assert.ok(f.coordinates);
					
					const f2 = fts[1];
					assert.equal(f2.status, 'nopics');
					assert.equal(f2.pictures.length, 0);
					assert.ok(f2.id != null);
					assert.ok(f2.coordinates);
					
					assert.equal(p1.statuses[res.token].ready, 2);
					assert.equal(p1.statuses[res.token].total, 2);
					assert.equal(p1.getRetrievalStatus(res.token), 100);
					
					done();
				})
				.catch(e => {
					assert.fail(e);
					done();
				});
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		}).timeout(TIMEOUT * 2);
	});
	
	describe("preparePicturesForMission", () => {
		let mid = null;
		
		it("retrieves pictures correctly", done => {
			//Create mission
			db.query(
				"INSERT INTO mission(type, theme,maxYears, areaname, shortdesc, fulldesc, datatype, dataoptions, userid, username, geom) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, ST_SetSRID(ST_MakePolygon($10), 4326)) RETURNING id",
				missionParams
			)
			.then(d => {
				mid = d.rows[0].id;
				
				//Create features
				db.query("INSERT INTO feature(mission, geom, geomfull) VALUES ($1, ST_SetSRID(ST_Point($2, $3), 4326), ST_SetSRID(ST_Point($2, $3), 4326))", [ mid, lng3, lat3 ])
				.then(() => {
					db.query("INSERT INTO feature(mission, geom, geomfull) VALUES ($1, ST_SetSRID(ST_Point($2, $3), 4326), ST_SetSRID(ST_Point($2, $3), 4326))", [ mid, lng4, lat4 ])
					.then(() => {
					
						//Retrieve pictures for mission
						const p1 = new PicStore();
						const res = p1.preparePicturesForMission(mid);
						assert.equal(p1.getRetrievalStatus(res.token), 0);
						
						res.ready.then(() => {
							//Get pictures for the mission
							db.query("SELECT f.id, f.status, f.pictures FROM feature f WHERE f.mission = $1 ORDER BY f.id", [ mid ])
							.then(d2 => {
								
								//Check
								assert.equal(d2.rows.length, 2);
								
								const f = d2.rows[0];
								assert.ok(f.pictures !== null);
								assert.ok(f.pictures.length > 0);
								assert.equal(f.status, "new");
								
								const f2 = d2.rows[1];
								assert.ok(f2.pictures !== null);
								assert.equal(f2.pictures.length, 0);
								assert.equal(f2.status, "nopics");
								
								assert.equal(p1.statuses[res.token].ready, 2);
								assert.equal(p1.statuses[res.token].total, 2);
								assert.equal(p1.getRetrievalStatus(res.token), 100);
								
								done();
							})
							.catch(e => {
								assert.fail(e);
								done();
							});
						})
						.catch(e => {
							assert.fail(e);
							done();
						});
					})
					.catch(e => {
						assert.fail(e);
						done();
					});
				})
				.catch(e => {
					assert.fail(e);
					done();
				});
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		}).timeout(TIMEOUT * 2);
	});
});
