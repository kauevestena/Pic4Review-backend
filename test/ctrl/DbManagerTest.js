/*
 * Test class for DbManager
 */

const assert = require('assert');
const { Client } = require('pg');
const DbManager = require('../../src/ctrl/DbManager');

const CONFIG = require("../../config.json");
const DB_HOST = CONFIG.db.host;
const DB_USER = CONFIG.db.user;
const DB_PASS = CONFIG.db.pass;
const DB_NAME = CONFIG.db.name;
const DB_PORT = CONFIG.db.port;

const dbClient = new Client({
	user: DB_USER,
	host: DB_HOST,
	database: DB_NAME,
	password: DB_PASS,
	port: DB_PORT
});

describe('Ctrl > DbManager', () => {
	describe("Constructor", () => {
		it("works", () => {
			const db = new DbManager(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			db.stop();
			assert.ok(db.pool !== null);
		});
		
		it("fails if no host given", () => {
			assert.throws(
				() => { new DbManager(); },
				TypeError
			);
		});
		
		it("fails if no user given", () => {
			assert.throws(
				() => { new DbManager(DB_HOST); },
				TypeError
			);
		});
		
		it("fails if no db name given", () => {
			assert.throws(
				() => { new DbManager(DB_HOST, DB_USER, DB_PASS); },
				TypeError
			);
		});
	});
	
	describe('initDb', () => {
		it("works", done => {
			const db = new DbManager(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			db.initDb()
			.then(() => {
				dbClient.connect();
				dbClient.query('SELECT * FROM edit')
				.then(res => {
					assert.ok(true);
					db.stop();
					done();
				})
				.catch(e => {
					assert.fail(e);
					db.stop();
					done();
				});
			})
			.catch(e => {
				assert.fail(e);
				db.stop();
				done();
			});
		});
	});
	
	describe('logHttpQuery', () => {
		it('logs query', (done) => {
			const db = new DbManager(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			db.initDb()
			.then(() => {
				db.logHttpQuery({
					ip: "0.0.0.0",
					path: "/status",
					query: {
						user: "PanierAvide",
						param: "true"
					}
				}, null, () => {
					assert.ok(true);
					setTimeout(() => {
						db.stop();
						done();
					}, 1000);
				});
			})
			.catch(e => {
				assert.fail(e);
				db.stop();
				done();
			});
		});
	});
});
