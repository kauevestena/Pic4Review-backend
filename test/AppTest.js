/**
 * Tests for App, in particular for testing routes.
 */

const assert = require('assert');
const request = require('supertest');
const CONST = require('../src/model/Constants');
const fs = require("fs");
const xml = require("libxmljs");
const P4C = require("pic4carto");

const APP_PATH = '../src/App';
const TIMEOUT = 10000;

describe('App (API routes)', function() {
	var server;
	let midNext = null;
	
	//Setting up server
	beforeEach(function(done) {
		delete require.cache[require.resolve(APP_PATH)];
		delete global.db;
		server = require(APP_PATH);
		
		if(!server.p4rReady) {
			const timer = setInterval(() => {
				if(server.p4rReady) {
					clearInterval(timer);
					done();
				}
			}, 200);
		}
	});
	
	//Stopping server
	afterEach(function(done) {
		server.close(done);
	});
	
	describe('/', function() {
		it("should return status on GET", function(done){
			request(server)
			.get("/")
			.expect("Content-type",/json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.status, "OK");
				done();
			});
		});
	});
	
	describe('/missions', function() {
		it("should return missions list on GET", function(done){
			request(server)
			.get("/missions")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.missions.length > 0);
				assert.ok(res.body.missions.length <= 10);
				assert.ok(res.body.missions[0].total !== undefined);
				assert.ok(res.body.missions[0].new !== undefined);
				assert.ok(res.body.missions[0].created !== undefined);
				done();
			});
		});
		
		it("should create mission on POST for Osmose", done => {
			const data = {
				type: "fix",
				theme: "amenity",
				maxYears: 3,
				areaname: "Rennes, France, Europe",
				shortdesc: "Test mission",
				fulldesc: "Long description of this mission, which main goal is to verify if API works properly !",
				datatype: "osmose",
				dataoptions: { item: 8210, amount: 5 },
				minlat: 47.8482,
				minlon: -2.0682,
				maxlat: 48.3599,
				maxlon: -1.1838,
				username: "user3",
				userid: 3
			};
			
			request(server)
			.post("/missions")
			.send(data)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body.error);
					assert.fail(err);
				}
				
				assert.equal(res.status, 200);
				assert.equal(res.body.info, "Features being prepared for this mission");
				assert.ok(res.body.id > 0);
				console.log(res.body.pictoken);
				assert.ok(res.body.pictoken >= 0);
				done();
			});
		}).timeout(TIMEOUT * 2);
		
		it("should create mission on POST for Overpass", done => {
			const data = {
				type: "fix",
				theme: "amenity",
				maxYears: 3,
				areaname: "Rennes, France, Europe",
				shortdesc: "Test mission",
				fulldesc: "Long description of this mission, which main goal is to verify if API works properly !",
				datatype: "overpass",
				dataoptions: {
					query: '[out:json][timeout:25];(node["highway"="traffic_signals"](48.12721801169093,-1.686323583126068,48.127291414673245,-1.6862136125564575););out body;',
					geojson: JSON.stringify({"type":"FeatureCollection","features":[
						{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-1.68330,48.12790]}},
						{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[0.1,0.1]}},
						{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[0.2,0.2]}}
					]})
				},
				minlat: 48.1278,
				minlon: -1.6834,
				maxlat: 48.1280,
				maxlon: -1.6832,
				username: "user3",
				userid: 3
			};
			
			request(server)
			.post("/missions")
			.send(data)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					assert.fail(res.body.error || err);
				}
				
				assert.equal(res.status, 200);
				assert.equal(res.body.info, "Features being prepared for this mission");
				assert.ok(res.body.id > 0);
				midNext = res.body.id;
				assert.ok(res.body.pictoken >= 0);
				done();
			});
		}).timeout(TIMEOUT * 2);
		
		it("should create mission on POST for Detections", done => {
			const data = {
				type: "integrate",
				theme: "amenity",
				maxYears: 3,
				areaname: "Rennes, France, Europe",
				shortdesc: "Test mission",
				fulldesc: "Long description of this mission, which main goal is to verify if API works properly !",
				datatype: "detections",
				dataoptions: { type: P4C.Detection.SIGN_STOP },
				minlat: 48.22281,
				minlon: -1.78230,
				maxlat: 48.22438,
				maxlon: -1.78032,
				username: "user3",
				userid: 3
			};
			
			request(server)
			.post("/missions")
			.send(data)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body.error);
					assert.fail(err);
				}
				
				assert.equal(res.status, 200);
				assert.equal(res.body.info, "Features being prepared for this mission");
				assert.ok(res.body.id > 0);
				assert.ok(res.body.pictoken >= 0);
				done();
			});
		}).timeout(TIMEOUT * 2);
	});
	
	describe('/missions/templates', function() {
		it("should return templates list on GET", function(done){
			request(server)
			.get("/missions/templates")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.templates.length > 0);
				assert.ok(res.body.templates[0].id !== undefined);
				assert.ok(res.body.templates[0].theme !== undefined);
				assert.ok(res.body.templates[0].type !== undefined);
				assert.ok(res.body.templates[0].shortdesc !== undefined);
				assert.ok(res.body.templates[0].fulldesc !== undefined);
				done();
			});
		});
	});
	
	describe("/missions/loading", () => {
		it("works", done => {
			request(server)
			.get("/missions/loading?pictoken=0")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.loading >= 0);
				done();
			});
		});
	});
	
	describe('/missions/map', function() {
		it("should return missions list on GET", function(done){
			request(server)
			.get("/missions/map")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.geojson.features.length > 0);
				assert.equal(res.body.geojson.features[0].geometry.coordinates.length, 2);
				assert.ok(Object.keys(res.body.geojson.features[0].properties).length > 0);
				done();
			});
		});
	});
	
	describe('/missions/preview', () => {
		let nb = 0;
		
		it("should return features preview for Osmose on POST", done => {
			const opts = {
				datatype: "osmose",
				dataoptions: { item: 8210 },
				minlat: 47.8482,
				minlon: -2.0682,
				maxlat: 48.3599,
				maxlon: -1.1838,
			};
			request(server)
			.post("/missions/preview")
			.send(opts)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length > 0);
				nb = res.body.features.length;
				
				res.body.features.forEach(f => {
					assert.ok(f.status === "nopics" || f.status === "new");
					assert.ok(f.pictures);
					assert.ok(f.id);
				});
				done();
			});
		}).timeout(TIMEOUT * 10);
		
		it("should return features preview for Detections on POST", done => {
			const opts = {
				datatype: "detections",
				dataoptions: { type: P4C.Detection.SIGN_STOP },
				minlat: 48.22281,
				minlon: -1.78230,
				maxlat: 48.22438,
				maxlon: -1.78032
			};
			request(server)
			.post("/missions/preview")
			.send(opts)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length > 0);
				
				res.body.features.forEach(f => {
					assert.ok(f.status === "nopics" || f.status === "new");
					assert.ok(f.pictures);
					assert.ok(f.id);
				});
				done();
			});
		}).timeout(TIMEOUT * 10);
		
		it("should return features preview for Overpass on POST", done => {
			const opts = {
				dataoptions: {
					geojson: {"type":"FeatureCollection","features":[{"type":"Feature","properties":{"k1":"v1"},"geometry":{"type":"Point","coordinates":[-1.68330,48.12790]}}]}
				},
				datatype: "overpass",
				minlat: 48.1006,
				minlon: -1.6936,
				maxlat: 48.1265,
				maxlon: -1.6678
			};
			
			request(server)
			.post("/missions/preview")
			.send(opts)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length, 1);
				
				res.body.features.forEach(f => {
					assert.ok(f.status === "nopics" || f.status === "new");
					assert.ok(f.pictures);
					assert.ok(f.id);
				});
				done();
			});
		}).timeout(TIMEOUT * 10);
		
		it("should work using picture cache", done => {
			const opts = {
				datatype: "osmose",
				dataoptions: { item: 8210 },
				minlat: 47.8482,
				minlon: -2.0682,
				maxlat: 48.3599,
				maxlon: -1.1838,
			};
			
			request(server)
			.post("/missions/preview")
			.send(opts)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length > 0);
				assert.equal(res.body.features.length, nb);
				
				res.body.features.forEach(f => {
					assert.ok(f.status === "nopics" || f.status === "new");
					assert.ok(f.pictures);
					assert.ok(f.id);
				});
				done();
			});
		}).timeout(TIMEOUT * 2);
		
		it("handles large features set on POST", done => {
			const opts = {
				dataoptions: {
					geojson: fs.readFileSync("./test/assets/crossing.geojson", "utf-8")
				},
				datatype: "overpass",
				minlat: 48.1006,
				minlon: -1.6936,
				maxlat: 48.1265,
				maxlon: -1.6678
			};
			
			request(server)
			.post("/missions/preview")
			.send(opts)
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.error(err);
					console.log(res);
					assert.fail(err);
					done();
				}
				
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length, 100);
				assert.ok(res.body.limited);
				
				res.body.features.forEach(f => {
					assert.ok(f.status === "nopics" || f.status === "new");
					assert.ok(f.pictures);
					assert.ok(f.id);
				});
				done();
			});
		}).timeout(TIMEOUT * 10);
	});
	
	describe('/missions/:mid', function() {
		it("should return mission on GET", function(done){
			request(server)
			.get("/missions/1")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.mission !== null);
				assert.equal(res.body.mission.id, "1");
				done();
			});
		});
		
		it("should return mission on GET with canEdit true if author", function(done){
			request(server)
			.get("/missions/1?userid=1")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.mission !== null);
				assert.equal(res.body.mission.id, "1");
				assert.ok(res.body.canEdit === true);
				done();
			});
		});
		
		it("should return mission on GET with canEdit if not author", function(done){
			request(server)
			.get("/missions/1?userid=2")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.mission !== null);
				assert.equal(res.body.mission.id, "1");
				assert.ok(res.body.canEdit === false);
				done();
			});
		});
		
		it("should return mission on GET with canEdit if admin", function(done){
			request(server)
			.get("/missions/1?userid=1337")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.mission !== null);
				assert.equal(res.body.mission.id, "1");
				assert.ok(res.body.canEdit === true);
				done();
			});
		});
		
		it("should update mission on PUT", done => {
			const data = {
				type: "fix",
				theme: "amenity",
				maxYears: 3,
				areaname: "Rennes, France, Europe",
				shortdesc: "Test mission",
				fulldesc: "Long description of this mission, which main goal is to verify if API works properly !",
				datatype: "osmose",
				dataoptions: { item: 8180, class: 2, amount: 1 },
				minlat: 48.0769155,
				minlon: -1.7525876,
				maxlat: 48.1549705,
				maxlon: -1.6244045,
				username: "user3",
				userid: 3
			};
			
			request(server)
			.post("/missions")
			.send(data)
			.end(function(err, res1){
				if(err) {
					console.log("error 1", res1.body.error);
					assert.fail(err);
				}
				
				const mid = res1.body.id;
				const update = {
					userid: 3,
					status: "online",
					dataoptions: { item: 8180, class: 2, amount: 1, editors: { type: "disabled" } }
				};
				
				//Update status
				request(server)
				.put("/missions/"+mid)
				.send(update)
				.expect("Content-type", /json/)
				.expect(200)
				.end((err, res2) => {
					if(err) {
						console.log("error 2", res2.body.error);
						assert.fail(err);
					}
					
					assert.equal(res2.body.info, "Mission updated");
					
					const update2 = {
						userid: 3,
						type: "integrate",
						theme: "nature", //NOT SURE: it may not have to appear here, right?
						areaname: "Rennes, Bretagne"
					};
					
					//Update description
					request(server)
					.put("/missions/"+mid)
					.send(update2)
					.expect("Content-type", /json/)
					.expect(200)
					.end((err, res3) => {
						if(err) {
							console.log("error 3", res3.body.error);
							assert.fail(err);
						}
						
						assert.equal(res3.body.info, "Mission updated");
						
						//Check data
						request(server)
						.get("/missions/"+mid)
						.expect("Content-type", /json/)
						.expect(200)
						.end((err, res4) => {
							if(err) {
								console.log("error 4", res4.body.error);
								assert.fail(err);
							}
							
							assert.equal(res4.body.mission.id, mid);
							assert.equal(res4.body.mission.type, "integrate");
							assert.equal(res4.body.mission.theme, "nature");
							// assert.equal(res4.body.mission.maxYears, 3);
							assert.equal(res4.body.mission.areaname, "Rennes, Bretagne");
							assert.equal(res4.body.mission.dataoptions.editors.type, "disabled");
							
							done();
						});
					});
				});
			});
		}).timeout(TIMEOUT * 3);
		
		it("should update mission on PUT with admin account", done => {
			const data = {
				type: "fix",
				theme: "amenity",
				maxYears: 3,
				areaname: "Rennes, France, Europe",
				shortdesc: "Test mission",
				fulldesc: "Long description of this mission, which main goal is to verify if API works properly !",
				datatype: "osmose",
				dataoptions: { item: 8180, class: 2, amount: 1 },
				minlat: 48.0769155,
				minlon: -1.7525876,
				maxlat: 48.1549705,
				maxlon: -1.6244045,
				username: "user3",
				userid: 3
			};
			
			request(server)
			.post("/missions")
			.send(data)
			.end(function(err, res1){
				if(err) {
					console.log(res1.body.error);
					assert.fail(err);
				}
				
				const mid = res1.body.id;
				
				//Update
				request(server)
				.put("/missions/"+mid)
				.send({ userid: 1337, status: "canceled" })
				.expect("Content-type", /json/)
				.expect(200)
				.end((err, res2) => {
					if(err) {
						console.log("error 1", res2.body.error);
						assert.fail(err);
					}
					
					assert.equal(res2.body.info, "Mission updated");
					
					//Try with other account
					request(server)
					.put("/missions/"+mid)
					.send({ userid: 403, status: "online" })
					.expect("Content-type", /json/)
					.expect(403)
					.end((err, res3) => {
						if(res3.status === 403) {
							assert.ok(true);
						}
						else if(err) {
							assert.fail(err);
						}
						else {
							assert.fail("Should not succeed");
						}
						
						done();
					});
				});
			});
		}).timeout(TIMEOUT * 3);
	});
	
	describe('/missions/:mid/features', function() {
		it("should return mission features on GET", function(done){
			request(server)
			.get("/missions/1/features")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.features.length > 0);
				done();
			});
		}).timeout(TIMEOUT * 3);
	});
	
	describe('/missions/:mid/features/next', function() {
		it("should return mission next feature on GET", function(done){
			request(server)
			.get("/missions/"+midNext+"/features/next")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.feature.id !== null);
				assert.ok(res.body.feature.geomfull !== null);
				done();
			});
		}).timeout(TIMEOUT);
		
		it("should work with coordinates on GET", function(done){
			request(server)
			.get("/missions/"+midNext+"/features/next?lat=48.1172&lng=-1.6824")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.feature.id !== null);
				done();
			});
		}).timeout(TIMEOUT);
		
		it("should not send skipped item on GET", function(done){
			request(server)
			.put("/missions/1/features/4?userid=1337&username=admin1&status=skipped")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				
				request(server)
				.get("/missions/1/features/next?lat=-12&lng=-12&userid=1337")
				.expect("Content-type", /json/)
				.expect(200)
				.end(function(err, res){
					assert.equal(res.status, 200);
					assert.ok(res.body.feature.id !== null);
					assert.ok(res.body.feature.geom.coordinates[0] !== -12);
					assert.ok(res.body.feature.geom.coordinates[1] !== -12);
					done();
				});
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/missions/:mid/features/:fid', function() {
		it("should update on PUT", function(done){
			request(server)
			.put("/missions/1/features/1?userid=1234&username=PanierAvide&status=reviewed")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/missions/:mid/export/missing', () => {
		it("works for geojson", done => {
			request(server)
			.get("/missions/1/export/missing?format=geojson")
			.expect("Content-type", "application/force-download")
			.expect("Content-disposition", "attachment; filename=missing.geojson")
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body);
					assert.fail(err);
				}
				
				assert.equal(res.status, 200);
				try {
					const r = JSON.parse(res.text);
					assert.ok(r.features.length > 0);
					assert.equal(r.features[0].geometry.type, "Point");
					assert.ok(!isNaN(r.features[0].geometry.coordinates[0]));
					assert.ok(!isNaN(r.features[0].geometry.coordinates[1]));
					assert.ok(r.features[0].properties.name);
				}
				catch(e) {
					assert.fail(e);
				}
				done();
			});
		}).timeout(TIMEOUT);
		
		it("works for gpx", done => {
			const xsd = xml.parseXmlString(fs.readFileSync("./test/assets/gpx.xsd"));
			
			request(server)
			.get("/missions/1/export/missing?format=gpx")
			.expect("Content-type", "application/force-download")
			.expect("Content-disposition", "attachment; filename=missing.gpx")
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body);
					assert.fail(err);
				}
				
				assert.equal(res.status, 200);
				const doc = xml.parseXmlString(res.text);
				assert.ok(doc.validate(xsd));
				done();
			});
		}).timeout(TIMEOUT);
		
		it("works for kml", done => {
			const xsd = xml.parseXmlString(fs.readFileSync("./test/assets/kml.xsd"), { baseUrl: "./test/assets/" });
			
			request(server)
			.get("/missions/1/export/missing?format=kml")
			.expect("Content-type", "application/force-download")
			.expect("Content-disposition", "attachment; filename=missing.kml")
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body);
					assert.fail(err);
				}
				
				assert.equal(res.status, 200);
				const doc = xml.parseXmlString(res.text);
				assert.ok(doc.validate(xsd));
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/missions/:mid/stats', function() {
		it("should return mission stats on GET", function(done){
			request(server)
			.get("/missions/1/stats")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.users.length > 0);
				assert.ok(Object.keys(res.body.days).length > 0);
				assert.ok(Object.keys(res.body.status).length > 0);
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/users/:uid/stats', function() {
		it("should return user stats on GET", function(done){
			request(server)
			.get("/users/1/stats")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.ok(res.body.featuresEdited > 0);
				assert.ok(res.body.missionsCreated > 0);
				assert.ok(res.body.themes.amenity > 0);
				assert.ok(res.body.themes.maxYears > 0);
				assert.ok(res.body.place > 0);
				assert.ok(Object.keys(res.body.amountEdits).length >= 1);
				
				Object.keys(res.body.amountEdits).forEach(k => {
					assert.ok(/^\d{4}-\d{2}-\d{2}$/.test(k));
					assert.ok(!isNaN(res.body.amountEdits[k]));
				});
				
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/users/stats', function() {
		it("should return users stats on GET", function(done){
			request(server)
			.get("/users/stats")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body);
					assert.fail(err);
				}
				assert.equal(res.status, 200);
				assert.ok(res.body.scores.length > 0);
				assert.ok(res.body.scores[0].user !== null);
				assert.ok(res.body.scores[0].featuresEdited > 0);
				assert.ok(Object.keys(res.body.amountEdits).length > 0);
				assert.ok(Object.keys(res.body.themes).length > 0);
				assert.ok(Object.keys(res.body.picProviders).length > 0);
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/users/dataviz', function() {
		it("should return instance stats on GET", function(done){
			request(server)
			.get("/users/dataviz")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				if(err) {
					console.log(res.body);
					assert.fail(err);
				}
				console.log(res.body);
				assert.equal(res.status, 200);
				assert.ok(res.body.missions > 0);
				assert.ok(res.body.users > 0);
				assert.ok(res.body.edits > 0);
				done();
			});
		}).timeout(TIMEOUT);
	});
	
	describe('/pictures/missing', function() {
		it("should return missing pictures on GET", function(done){
			request(server)
			.get("/pictures/missing")
			.expect("Content-type", /json/)
			.expect(200)
			.end(function(err, res){
				assert.equal(res.status, 200);
				assert.equal(res.body.type, "FeatureCollection");
				assert.ok(res.body.features.length > 0);
				done();
			});
		}).timeout(TIMEOUT);
	});
});
