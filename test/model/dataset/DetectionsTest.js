/*
 * Test script for model/dataset/Detections.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const Detections = require('../../../src/model/dataset/Detections');
const P4C = require('pic4carto');
const LatLng = P4C.LatLng;
const LatLngBounds = P4C.LatLngBounds;

const TIMEOUT = 10000;

describe("Model > Dataset > Detections", () => {
	describe("Constructor", () => {
		it("works with valid parameters", () => {
			const d1 = new Detections(P4C.Detection.SIGN_STOP, { bbox: new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515)) });
			assert.equal(d1.options.type, P4C.Detection.SIGN_STOP);
		});
		
		it("fails if no type is given", () => {
			assert.throws(() => {
				new Detections();
			}, TypeError);
		});
	});
	
	describe("getAllFeatures", () => {
		it("returns retrieved features", done => {
			const d2 = new Detections(P4C.Detection.SIGN_STOP, { bbox: new LatLngBounds(new LatLng(48.12902, -1.67708), new LatLng(48.13101, -1.67515)) });
			
			d2.getAllFeatures()
			.then(fts => {
				assert.ok(fts.length > 0);
				assert.ok(fts[0] !== null);
				assert.ok(Object.keys(fts[0].properties).length > 0);
				done();
			})
			.catch(e => {
				assert.fail(e);
				done();
			});
		}).timeout(TIMEOUT);
	});
});
